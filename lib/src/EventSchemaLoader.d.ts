import {FileSchemaLoaderStrategy} from "@themost/data";

declare class EventSchemaLoader extends FileSchemaLoaderStrategy {}

export {
    EventSchemaLoader
}
