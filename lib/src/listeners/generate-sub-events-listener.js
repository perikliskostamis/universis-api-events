import {parseExpression} from 'cron-parser';
import moment from 'moment';
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === 1) {
        if (!event.target.hasOwnProperty('eventHoursSpecification') || event.target.eventHoursSpecification === null) {
            return;
        }
        //get target event hours specification
        let thisEventHoursSpecification = typeof event.target.eventHoursSpecification !== 'object' ?
            await event.model.context.model('EventHoursSpecification')
                .where('id').equal(event.target.eventHoursSpecification)
                .getTypedItem() :
            event.model.context.model('EventHoursSpecification').convert(event.target.eventHoursSpecification);
        //fetch original data object
        const eventHoursSpecification = await event.model.context.model('EventHoursSpecification').where('id').equal(thisEventHoursSpecification.getId())
            .silent().getTypedItem();
        if (eventHoursSpecification) {
            //generate events
            let cronjob = eventHoursSpecification.toCronJobString();
            //get intervals
            let options = {
                // When currentDate matches given cron expression, cron-parser will miss current event
                // Subtract one second from the currentDate to avoid this
                // https://github.com/harrisiirak/cron-parser/issues/289
                currentDate: new Date(new Date(eventHoursSpecification.validFrom) - 1000),
                endDate: eventHoursSpecification.validThrough,
                iterator: true
            };
            let interval = parseExpression(cronjob, options);
            let intervals = [];
            while (interval) {
                try {
                    let obj = interval.next();
                    intervals.push(obj.value);
                } catch (err) {
                    break;
                }
            }


            // eslint-disable-next-line no-unused-vars
            const { id, dateCreated, dateModified, ..._event} = event.target;
            let events = intervals.map((x) => {
                return {
                    ..._event,
                    startDate: x.toDate(),
                    endDate: moment(x.toDate()).add(moment.duration(eventHoursSpecification.duration)).toDate(),
                    duration: eventHoursSpecification.duration,
                    superEvent: id,
                    eventHoursSpecification: null // sub-events shouldn't have eventHoursSpecification
                }
            });
            await event.model.context.model(event.target.additionalType).silent().save(events);
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
