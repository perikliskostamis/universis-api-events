const Place = require('../models/place-model');

export function afterExecute(event, callback) {
    return afterExecuteAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}


export async function afterExecuteAsync(event) {
    if (event.emitter && event.emitter.$expand && Array.isArray(event.emitter.$expand)) {
        // if availablePLaces are expanded apply the place flattener
        if (event.emitter.$expand.find(mapping => {
            // if specific properties of the mapping are selected don't flatten the results to avoid unexpected behavior
            return typeof mapping === 'object'
                ? mapping.name === 'availablePlaces' && !(mapping.options && mapping.options.$select)
                : mapping === 'availablePlaces'
        })) {
            const results = Array.isArray(event.result) ? event.result : [event.result]
            for (const result of results) {
                if (Array.isArray(result.availablePlaces) && result.availablePlaces.length) {
                    result.availablePlaces = await Place.getFlattenedPlaces(event.model.context, result.availablePlaces);
                }
            }
        }
    }
}
