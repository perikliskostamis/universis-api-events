import {ApplicationService} from '@themost/common';
import {ODataModelBuilder} from "@themost/data";
import {DataConfigurationStrategy} from '@themost/data';
import fs from 'fs';
import path from 'path';
import pluralize from 'pluralize';


export class EventService extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        this.install();
        // extend application service router
    }

    install() {
        // place your code here

    }

    // noinspection JSUnusedGlobalSymbols
    uninstall() {
        // place your code here
    }

}
