import {EdmMapping, DataObject} from '@themost/data';
import moment from 'moment'

/**
 * @class
 
 * @property {number} about
 * @property {Audience|any} audience
 * @property {EventHoursSpecification|any} eventHoursSpecification
 * @property {number} remainingAttendeeCapacity
 * @property {Date} startDate
 * @property {Date} endDate
 * @property {Date} doorTime
 * @property {User} contributor
 * @property {number} maximumAttendeeCapacity
 * @property {Department} organizer
 * @property {Array<User>} attendees
 * @property {string} inLanguage
 * @property {EventStatusType|any} eventStatus
 * @property {Event} superEvent
 * @property {string} duration
 * @property {Date} previousStartDate
 * @property {Place|any} location
 * @property {boolean} isAccessibleForFree
 * @property {User} performer
 * @augments {DataObject}
 */
@EdmMapping.entityType('Event')
class Event extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    static calculateDatesByTimeFrame(event, newTimeFrame, timeFrame) {
        const firstDayFirstWeek = moment(timeFrame.startDate).startOf('week');
        const startDate = moment(event.eventHoursSpecification ? event.eventHoursSpecification.validFrom : event.startDate);
        const startWeek = Math.floor(moment.duration(startDate.diff(firstDayFirstWeek)).asWeeks());

        // let the new startDate of the event be the startDate of the  new time frame and shift it accordingly
        const newStartDate = moment(newTimeFrame.startDate);
        if (startWeek === 0 && startDate.day() < newStartDate.day()) {
            //  The new time frame starts later in the week than when the event is supposed to start (e.g. the old time frame started on
            //  Mon and the event was on Wed but the new timetable starts on Thu) and therefore the event can't start on the same day of the first week
            // in the new timetable. If the event is a recurring one, sub-events for it should be created from the new timetable's start date and on (startDate isn't shifted).
            // Else it should be moved on the same day of the next week.
            if (!event.eventHoursSpecification) { newStartDate.add(1, 'week').day(startDate.day()); }
        } else {
            // If the new time frame starts earlier in the first week than when the event is supposed to start or the event is supposed to start on any week
            // other than the first, the event (recurring or not) should start on the same week and day as in the old time frame.
            newStartDate.add(startWeek, 'weeks').day(startDate.day());
        }

        return Event.calculateDatesByDate(event, newStartDate);
    }

    static calculateDatesByDate(event, newStart) {
        const getNewDates = (startDate, endDate) => {
            const start = moment(startDate);
            const end = moment(endDate);
            const weekSpan = Math.floor(moment.duration(end.diff(moment(start).startOf('week'))).asWeeks());

            const newStartDate = moment(newStart).hours(start.hours()).minutes(start.minutes());
            const newEndDate = moment(newStartDate).add(weekSpan, 'weeks').day(end.day()).hours(end.hours()).minutes(end.minutes());

            return {
                startDate: newStartDate.toDate(),
                endDate: newEndDate.toDate()
            }
        }

        event = JSON.parse(JSON.stringify(event))
        if (event.eventHoursSpecification) {
            const newDates = getNewDates(event.eventHoursSpecification.validFrom, event.eventHoursSpecification.validThrough)
            event.startDate = event.eventHoursSpecification.validFrom = newDates.startDate;
            event.endDate = event.eventHoursSpecification.validThrough = newDates.endDate;
            delete event.eventHoursSpecification.id
            delete event.eventHoursSpecification.identifier

            // opens, closes are Time fields not DateTime fields
            const toTimeString = value => value instanceof Date ? value.toTimeString().substring(0, 8) : value
            event.eventHoursSpecification.opens = toTimeString(event.eventHoursSpecification.opens);
            event.eventHoursSpecification.closes = toTimeString(event.eventHoursSpecification.closes);
            
            // months may have changed
            const startMonth = event.eventHoursSpecification.validFrom.getMonth() + 1;
            const endMonth = event.eventHoursSpecification.validThrough.getMonth() + 1;
            event.eventHoursSpecification.monthOfYear = Array(endMonth - startMonth + 1).fill(0).map((i, index) => index + startMonth).join(',');
        } else {
            const newDates = getNewDates(event.startDate, event.endDate)
            event.startDate = newDates.startDate;
            event.endDate = newDates.endDate;
        }

        return event
    }

}
module.exports = Event;
