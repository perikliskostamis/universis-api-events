import {EdmMapping, DataObject} from '@themost/data';

/**
 * @class
 
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('EventType')
class EventType extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = EventType;
