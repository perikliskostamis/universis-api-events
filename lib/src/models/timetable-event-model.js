import { EdmMapping, EdmType } from '@themost/data';
import { DataError } from "@themost/common";
const ejs = require('ejs');
const Event = require('./event-model');

/**
 * @class
 * @property {Array<Place>} availablePlaces
 */
@EdmMapping.entityType('TimetableEvent')
class TimetableEvent extends Event {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.param('data', 'Object', false, true)
    @EdmMapping.action('copy', 'CopyTimetableEventAction')
    async copy(data) {
        if (!data.academicYear || !data.startDate) {
            throw new DataError('E_INVALID_DATA', 'Missing required parameters. Either academic year or start date is missing.', null, 'TimetableEvent');
        }

        const action = await this.context.model('CopyTimetableEventAction').save({ timetableEvent: this.getId() });
        this.copyTimetable(action, data);
        return action;
    }

    async copyTimetable(action, data) {
        try {
            const timetable = await this.getModel()
                .where('id').equal(this.getId())
                .expand('availablePlaces', 'availableEventTypes', 'academicPeriods', 'examPeriods')
                .getItem();

            const timetableEvents = await this.context.model('Events')
                .where('superEvent')
                .equal(timetable.id)
                .expand('eventHoursSpecification')
                .take(-1)
                .getItems();


            // save new timetable
            // eslint-disable-next-line no-unused-vars
            const { id, dateModified, dateCreated, ...timetableBase } = timetable;
            const newTimetable = await this.getModel().save({
                ...Event.calculateDatesByDate(timetableBase, data.startDate),
                academicYear: typeof data.academicYear === 'object' ? data.academicYear.id : data.academicYear,
                description: data.description || null,
                name: data.name || `${timetable.name} - copy`,
                eventStatus: { alternateName: 'EventOpened' },
                eventHoursSpecification: null, // TODO: add eventHoursSpecification with except dates
            });

            const failures = [];
            for (const event of timetableEvents) {
                const getTimeFrame = ({ startDate, endDate }) => ({ startDate, endDate })
                const updatedEvent = Event.calculateDatesByTimeFrame({ ...event, superEvent: newTimetable.id }, getTimeFrame(newTimetable), getTimeFrame(timetable));
                try {
                    await this.context.model(event.additionalType).convert(event).addToTimetable(updatedEvent, newTimetable);
                } catch (error) {
                    failures.push({event, error})
                }
            }

            // failures template
            const template = `<% failures.forEach(function(failure){ -%>
                <% if(!failure.event.eventHoursSpecification){ -%>
                    <p>The event with title <strong><%- failure.event.name %></strong>, start date <strong><%- new Date(failure.event.startDate).toLocaleDateString('${this.context.locale}') %></strong>, end date <strong><%- new Date(failure.event.endDate).toLocaleDateString('${this.context.locale}') %></strong> could not be copied.<br/><strong>Reason</strong>: <%- failure.error.message -%></p>
                <% } else { -%>
                    <p>The recurring event with title <strong><%- failure.event.name %></strong>, which was valid from <strong><%- new Date(failure.event.eventHoursSpecification.validFrom).toLocaleDateString('${this.context.locale}') %></strong> through <strong><%- new Date(failure.event.eventHoursSpecification.validThrough).toLocaleDateString('${this.context.locale}') %></strong> and its sub-events could not be copied.<br/><strong>Reason</strong>: <%- failure.error.message -%></p>
                <% } -%>
            <% }); -%>`

            // after the timetable is copied, update actionStatus
            action.actionStatus = { alternateName: 'CompletedActionStatus' };
            action.endTime = new Date();
            action.result = newTimetable;
            action.failures = failures.length ? ejs.render(template, { failures }, { rmWhitespace: true }) : null;
            this.context.model('CopyTimetableEventAction').save(action);
        } catch (error) {
            // in case of error, update actionStatus as failed
            action.actionStatus = { alternateName: 'FailedActionStatus' };
            action.endTime = new Date();
            this.context.model('CopyTimetableEventAction').save(action);
            throw error
        }
    }

    @EdmMapping.func("availablePlaces", EdmType.CollectionOf("Places"))
    async getPlaces() {
        return this.getModel().where('id').equal(this.getId()).expand('availablePlaces').getItem().then(timetable => timetable.availablePlaces);
    }
}

module.exports = TimetableEvent;
